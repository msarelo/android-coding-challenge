# Android Coding Challenge


## Challenge


Develop an application, which will allow users to browse a list images from [Lorem Picsum](https://picsum.photos/). We expect that this application will support the following:


1. When the app launches, a loading 🌀 state should appear during the initial loading stage (e.g. API requests are made)

2. Once you've retrieved a valid list of images, we should display each in a 2-column grid of square images

3. Whenever the user taps on an image, a new screen (image detail) should appear with the following: A larger view of the image and the name of the image's author.

4. Additionally, the image detail screen should also include a button, which will allow the user to share the image with other apps (e.g. email, SMS, etc)


As time permits, consider some of the following:


* Adding basic test coverage

* Handling error states if the API fails to load

* Adding support for a caching layer to speed up cold launches

* Adding support for paging, so we automatically load more images as the user scrolls up/down

* Using different amounts of grid columns, based on if the phone is in portrait vs landscape mode

* Using different amounts of grid columns, based on if we're using an iPhone or iPad

* Adding support for anything else that you think would be useful...


### APIs


As part of this exercise, we anticipate that you'll need to access some of the following APIs:


* To list the first page of images: `GET https://picsum.photos/v2/list`. The API will return 30 items per page by default. To request another page, use the `?page` parameter.

* To fetch a specific image: `https://picsum.photos/200?image={IMAGE_ID}`

* And potentially any others as defined [on their home page](https://picsum.photos/)