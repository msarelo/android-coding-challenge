package pl.sarelo.androidcodingchallenge.domain.entities

sealed class ImagesResult {

    class Success(val data: List<Image> = emptyList()) : ImagesResult()

    class NetworkError(val error: Throwable) : ImagesResult()
}
