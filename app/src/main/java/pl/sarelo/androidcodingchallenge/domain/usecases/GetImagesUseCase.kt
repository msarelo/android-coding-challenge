package pl.sarelo.androidcodingchallenge.domain.usecases

import io.reactivex.Single
import pl.sarelo.androidcodingchallenge.core.async.SchedulerProvider
import pl.sarelo.androidcodingchallenge.core.db.DbImage
import pl.sarelo.androidcodingchallenge.core.db.ImageDao
import pl.sarelo.androidcodingchallenge.core.network.model.ApiImageModel
import pl.sarelo.androidcodingchallenge.domain.entities.Image
import pl.sarelo.androidcodingchallenge.domain.entities.ImageRepositoryResult
import pl.sarelo.androidcodingchallenge.domain.entities.ImagesRequest
import pl.sarelo.androidcodingchallenge.domain.entities.ImagesResult
import pl.sarelo.androidcodingchallenge.domain.repositories.ImageRepository

class GetImagesUseCase(
    private val networkImageRepository: ImageRepository,
    private val imageDao: ImageDao,
    schedulerProvider: SchedulerProvider
) : UseCaseWithParamsSingle<ImagesRequest, ImagesResult>(schedulerProvider) {

    override fun buildUseCase(useCaseParam: ImagesRequest): Single<ImagesResult> {
        return networkImageRepository.getImages(useCaseParam.pageNumber, useCaseParam.itemsPerPage)
            .flatMap {
                when (it) {
                    is ImageRepositoryResult.Success -> {
                        imageDao.insertImages(it.data.mapToDb())
                            .andThen(imageDao.getAllImages())
                            .map { ImagesResult.Success(it.mapToDomain()) }
                    }
                    is ImageRepositoryResult.NetworkError -> Single.just(
                        ImagesResult.NetworkError(
                            it.error
                        )
                    )
                }
            }
            .onErrorReturn { ImagesResult.NetworkError(it) }
    }
}

private fun List<ApiImageModel>.mapToDb(): List<DbImage> {
    return this.map { DbImage.create(it.id, it.author, it.url) }
}

private fun List<DbImage>.mapToDomain(): List<Image> {
    return this.map { Image(it.id, it.author, it.url) }
}
