package pl.sarelo.androidcodingchallenge.domain.entities

sealed class ImagesRequest(val pageNumber: Int, val itemsPerPage: Int) {

    class Network(pageNumber: Int, itemsPerPage: Int) :
        ImagesRequest(pageNumber, itemsPerPage)
}
