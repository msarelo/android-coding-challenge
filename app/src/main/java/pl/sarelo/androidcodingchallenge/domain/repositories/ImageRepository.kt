package pl.sarelo.androidcodingchallenge.domain.repositories

import io.reactivex.Single
import pl.sarelo.androidcodingchallenge.domain.entities.ImageRepositoryResult

interface ImageRepository {

    fun getImages(pageNumber: Int, itemsPerPage: Int): Single<ImageRepositoryResult>
}