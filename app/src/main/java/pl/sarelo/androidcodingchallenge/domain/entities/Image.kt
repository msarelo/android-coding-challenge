package pl.sarelo.androidcodingchallenge.domain.entities

data class Image(val id: Int, val author: String, val url: String)