package pl.sarelo.androidcodingchallenge.core.network.api

interface ApiFactory {
    fun <T> createRequest(serviceClass: Class<T>): T
}
