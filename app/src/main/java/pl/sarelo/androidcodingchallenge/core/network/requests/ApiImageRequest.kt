package pl.sarelo.androidcodingchallenge.core.network.requests

import io.reactivex.Single
import pl.sarelo.androidcodingchallenge.core.network.model.ApiImageModel
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface ApiImageRequest {

    @GET("v2/list")
    fun fetchImages(@QueryMap queryMap: Map<String, String>): Single<List<ApiImageModel>>
}
