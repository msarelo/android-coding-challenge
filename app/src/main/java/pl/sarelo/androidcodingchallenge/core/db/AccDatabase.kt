package pl.sarelo.androidcodingchallenge.core.db

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [DbImage::class], version = AccDatabase.VERSION, exportSchema = false)
abstract class AccDatabase : RoomDatabase() {
    abstract fun databaseDao(): ImageDao

    companion object {
        const val VERSION = 1
        const val DB_NAME = "AccDatabase"
        const val IMAGE_TABLE_NAME = "image"
    }
}