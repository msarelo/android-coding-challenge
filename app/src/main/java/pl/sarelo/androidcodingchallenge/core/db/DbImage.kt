package pl.sarelo.androidcodingchallenge.core.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = AccDatabase.IMAGE_TABLE_NAME, indices = [Index(value = ["id"], unique = true)])
data class DbImage(
    @ColumnInfo(name = "dbId")
    @PrimaryKey(autoGenerate = true)
    val dbId: Long?,
    val id: Int,
    val author: String,
    val url: String
) {
    companion object {
        fun create(
            id: Int,
            author: String,
            url: String
        ) = DbImage(null, id, author, url)
    }
}