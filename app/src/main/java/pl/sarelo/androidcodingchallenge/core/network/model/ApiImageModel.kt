package pl.sarelo.androidcodingchallenge.core.network.model

import pl.sarelo.androidcodingchallenge.domain.entities.Image

data class ApiImageModel(
    val id: Int,
    val author: String,
    val width: Int,
    val height: Int,
    val url: String,
    val downloadUrl: String
) {
    fun mapToDomain(): Image = Image(id, author, url)
}

