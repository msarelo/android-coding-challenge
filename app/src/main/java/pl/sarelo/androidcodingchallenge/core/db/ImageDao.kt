package pl.sarelo.androidcodingchallenge.core.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Completable
import io.reactivex.Single

@Dao
interface ImageDao {
    @Query("SELECT * from image")
    fun getAllImages(): Single<List<DbImage>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertImages(surveys: List<DbImage>): Completable
}