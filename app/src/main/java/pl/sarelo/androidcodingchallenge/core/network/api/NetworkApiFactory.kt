package pl.sarelo.androidcodingchallenge.core.network.api

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class NetworkApiFactory(private val baseUrl: String, private val okHttpClient: OkHttpClient) :
    ApiFactory {

    private var retrofit = buildRestClient()

    override fun <T> createRequest(serviceClass: Class<T>): T = retrofit.create(serviceClass)

    private fun buildRestClient() = Retrofit.Builder()
        .baseUrl(baseUrl)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
}
