package pl.sarelo.androidcodingchallenge.feature.home.grid

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import pl.sarelo.androidcodingchallenge.BR
import pl.sarelo.androidcodingchallenge.R
import pl.sarelo.androidcodingchallenge.domain.entities.Image

class GridAdapter(val actionWhenClick: (image: Image, view: View) -> Unit) :
    RecyclerView.Adapter<BaseViewHolder>() {

    private val asyncDiffer = AsyncListDiffer(this, DiffCallback())

    private fun getItemForPosition(position: Int) = asyncDiffer.currentList[position]

    override fun getItemCount() = asyncDiffer.currentList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val binding: ViewDataBinding =
            DataBindingUtil.inflate(LayoutInflater.from(parent.context), viewType, parent, false)
        return BaseViewHolder(binding)
    }

    override fun getItemViewType(position: Int) = R.layout.rv_grid_item

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        val image = getItemForPosition(position)
        holder.bind(mapOf(BR.image to image))

        holder.itemView.findViewById<View>(R.id.image).also {
            it.setOnClickListener { v ->
                actionWhenClick(image, v)
            }
        }
    }

    fun addItems(list: List<Image>) {
        asyncDiffer.submitList(list)
    }

    private class DiffCallback : DiffUtil.ItemCallback<Image>() {
        override fun areItemsTheSame(oldItem: Image, newItem: Image) = newItem.id == oldItem.id

        override fun areContentsTheSame(oldItem: Image, newItem: Image) = oldItem == newItem
    }
}
