package pl.sarelo.androidcodingchallenge.feature.home.grid

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

class BaseViewHolder(private val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(bindingVariables: Map<Int, Any>) {
        for ((key, value) in bindingVariables) {
            binding.setVariable(key, value)
        }
        binding.executePendingBindings()
    }
}
