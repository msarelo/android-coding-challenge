package pl.sarelo.androidcodingchallenge.feature.home

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import pl.sarelo.androidcodingchallenge.domain.entities.Image
import pl.sarelo.androidcodingchallenge.domain.entities.ImagesRequest
import pl.sarelo.androidcodingchallenge.domain.entities.ImagesResult
import pl.sarelo.androidcodingchallenge.domain.usecases.GetImagesUseCase


class MainActivityViewModel(private val getImagesUseCase: GetImagesUseCase) : ViewModel() {

    companion object {
        private const val TAG = "MainActivityViewModel"
        private const val START_PAGE = 0
        const val ITEMS_PER_PAGE = 30
    }

    val loading = MutableLiveData<Boolean>(true)
    val showList = MutableLiveData<Boolean>(false)
    val images = MutableLiveData<List<Image>>()
    val error = MutableLiveData<Throwable>()

    private var getImagesDisposable: Disposable? = null
    private var currentPage = START_PAGE

    init {
        images.observeForever() {
            showList.postValue(it.isEmpty().not())
        }
    }

    fun loadData() {
        sendRequest(ImagesRequest.Network(currentPage, ITEMS_PER_PAGE))
    }

    private fun sendRequest(imagesRequest: ImagesRequest) {

        getImagesDisposable?.dispose()
        getImagesDisposable = getImagesUseCase.execute(imagesRequest)
            .doOnSubscribe {
                loading.value = true
            }
            .doOnError {
                loading.value = false
            }
            .doOnSuccess {
                loading.value = false
            }
            .subscribeBy {
                Log.d(TAG, "data: ${it}")
                when (it) {
                    is ImagesResult.Success -> images.postValue(it.data)
                    is ImagesResult.NetworkError -> error.postValue(it.error)
                }

            }
    }

    override fun onCleared() {
        getImagesDisposable?.dispose()
        super.onCleared()
    }

    fun loadMoreData() {
        if (loading.value == false) {
            currentPage++
            sendRequest(ImagesRequest.Network(currentPage, ITEMS_PER_PAGE))
        }
    }

}