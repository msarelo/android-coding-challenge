package pl.sarelo.androidcodingchallenge.common.databinding

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

@BindingAdapter("android:visibility")
fun setVisibility(view: View, boolean: Boolean) {
    view.visibility = when (boolean) {
        true -> View.VISIBLE
        false -> View.GONE
    }
}

@BindingAdapter("loadImage")
fun loadImage(imageView: ImageView, imageId: Int) {

    val imageUrl = "https://picsum.photos/200?image=${imageId}"
    imageView.setImageDrawable(null)
    Glide.with(imageView)
        .asDrawable()
        .load(imageUrl)
        .into(imageView)
}
